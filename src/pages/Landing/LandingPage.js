import React, { useState } from "react";
import { useSelector, useDispatch } from 'react-redux'
import { urlSong } from "../../utils/constants/audio.cons";
import NavBarLanding from "../../components/navigation/Navbar/Navbar";
import FloatingButton from "../../components/FloatingButton/index";
import { BsFillCaretRightFill, BsFillPauseFill } from "react-icons/bs";
import {
  play,
  pause
} from './LandingPageSlice'
const Landing = (props) => {
  const [audio] = useState(new Audio(urlSong));
  const isPlaying = useSelector(state=>state.landingMusicSlice.isPlaying)
  const dispatch = useDispatch()

  const playSong = () => {
    dispatch(play())
    audio.play();
  };
  const pauseSong = () => {
    dispatch(pause())
    audio.pause();
  };

  return (
    <div>
      <NavBarLanding />
      <FloatingButton action={isPlaying ? pauseSong: playSong }>
        { isPlaying ?  <BsFillPauseFill color="black" size="30" />:  <BsFillCaretRightFill color="black" size="30" /> }
      </FloatingButton>
    </div>
  );
};
export default Landing;
