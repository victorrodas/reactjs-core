import React from "react";
import { Navbar, Container, Nav } from "react-bootstrap";
import { menuNavbar } from "../../../utils/constants/menu.cons";
import { NavBarStyled } from "./Navbar.styled";

const NavBarLanding = (props) => {
  return (
    <div>
      <NavBarStyled bg="light" expand="lg">
        <Container>
          <Navbar.Brand href="#home">React Test</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto"></Nav>
            <Nav>
              {menuNavbar.map((item, key) => {
                return (
                  <Nav.Link key={`menu-${key}`} href={item["url"]}>
                    {item["title"]}
                  </Nav.Link>
                );
              })}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </NavBarStyled>
    </div>
  );
};

export default NavBarLanding;
