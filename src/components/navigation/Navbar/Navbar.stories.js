
import React from 'react';
import NavbarLanding from './Navbar';
import 'bootstrap/dist/css/bootstrap.min.css';

export default {
  component: NavbarLanding,
  title: 'Components/NavbarLanding',
};

const Template = (args) => <NavbarLanding />;

export const Primary = Template.bind({});