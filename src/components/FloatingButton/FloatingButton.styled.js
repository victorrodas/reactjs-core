import styled from "styled-components";

export const FloatingButtonStyled = styled.button`
  background-color: #fffff;
  width: 60px;
  height: 60px;
  border-radius: 100%;
  background: #fffff;
  border: none;
  outline: none;
  color: #fff;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  transition: 0.3s;
  -webkit-tap-highlight-color: rgba(0, 0, 0, 0);
`;

export const ContainerButtonFloating = styled.div`
  position: absolute;
  right: 20px;
  bottom: 20px;
`;
