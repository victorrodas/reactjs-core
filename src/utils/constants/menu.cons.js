export const menuNavbar = [
    {
        'url':'/',
        'title':'Home'
    },
    {
        'url':'/about',
        'title':'About'
    },
    {
        'url':'/contact',
        'title':'Contact'
    },
]