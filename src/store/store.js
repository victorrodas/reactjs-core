import { configureStore } from "@reduxjs/toolkit";
import landingMusicSlice from "../pages/Landing/LandingPageSlice";
export const store = configureStore({
  reducer:{
    landingMusicSlice
  }
});
