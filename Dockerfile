FROM node:12-alpine

ENV GOSU_VERSION 1.14

COPY . /app

WORKDIR /app

RUN yarn install

#RUN yarn build

EXPOSE 3000

CMD ["node"]
